#
# Copyright 2013 Thomas Fischer <fischer@unix-ag.uni-kl.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use warnings;

use LWP::Simple;
use XML::LibXML;
use GIS::Distance;
use Text::Levenshtein::XS qw/distance/;

use bignum;    # Necessary for Vasttrafik's large node IDs

my $startlat         = 58.2553;    # Example value
my $startlon         = 13.7947;    # Example value
my $margin           = 0.0005;
my $vasttrafikapikey = undef;
my $scopedistance    = 0;

# Allow printing of non-ASCII characters
binmode( STDOUT, ":utf8" );
binmode( STDERR, ":utf8" );

# Used to compute distance between two coordinates
my $gis = GIS::Distance->new();

if ( $#ARGV == 0 ) {

    # If one parameter is supplied, interprete this as
    # Vasttrafik key (file or key itself)
    # This requires that starting coordinates got defined above!
    $vasttrafikapikey = $ARGV[0];
}
elsif ( $#ARGV == 1 ) {

    # If two parameters are supplied, interprete those as
    # starting latitude and starting longitude
    # This requires that $vasttrafikapikey is defined above!
    $startlat = $ARGV[0];
    $startlon = $ARGV[1];
}
elsif ( $#ARGV == 2 ) {

    # If three parameters are supplied, interprete those as
    # Vasttrafik key (file or key itself), starting latitude,
    # and starting longitude
    $vasttrafikapikey = $ARGV[0];
    $startlat         = $ARGV[1];
    $startlon         = $ARGV[2];
}

# Provided parameter for Vasttrafik is a file?
if ( defined($vasttrafikapikey) and -e $vasttrafikapikey ) {
    open my $f, $vasttrafikapikey or die "Could not open $vasttrafikapikey: $!";
    $vasttrafikapikey = <$f>;
    chomp($vasttrafikapikey);
}

# Check that the Vasttrafik API key is defined
die "Vasttrafik API key is not defined or inaccessible" unless ( defined($vasttrafikapikey) );

# Check that provided Vasttrafik API key looks real
die "Vasttrafik API key does not look like a key" unless ( $vasttrafikapikey =~ m/^[a-f0-9-]+$/ );

print "Used Vasttrafik API key: $vasttrafikapikey\n";
printf "Start location is  lat= %.5f  lon= %.5f\n", $startlat, $startlon;

# Print the hash associtated with a station containing station's name and location
sub printstation {
    my $station = $_[0];
    printf "%30s at  lat= %.5f  lon= %.5f  (distance= %.0f )\n", $station->{name}, $station->{lat}, $station->{lon}, $gis->distance( $startlat, $startlon => $station->{lat}, $station->{lon} )->meters();
}

# Return a link to OSM's slippy map with a marker at the coordinates
sub printurl {
    my ( $lat, $lon ) = @_;
    return sprintf "http://www.openstreetmap.org/?mlat=%.6f&mlon=%.6f&zoom=17&layers=M", $lat, $lon;
}

# Determine if two stations' names are equal based on substring match and Levenshtein distance
sub namesdomatch {
    my ( $a, $b ) = @_;
    return 1 if ( index( $a, $b ) >= 0 or index( $b, $a ) >= 0 );
    my $ldistance = distance( $a, $b );
    return 1 if ( $ldistance < 3 );
    return 0;
}

# Fetch stations near start coordinates
sub fetchvasttrafikstationsatcoordinates {
    my ( $lat, $lon ) = @_;
    my $numstations = 15;
    my $url         = "http://api.vasttrafik.se/bin/rest.exe/v1/location.nearbystops?authKey=" . $vasttrafikapikey . "&format=xml&originCoordLat=" . $lat . "&originCoordLong=" . $lon . "&maxNo=" . $numstations;
    print "Fetching URL \"" . $url . "\"\n";

    my $file = "/tmp/vasttrafik.xml";
    my $ret = mirror( $url, $file );
    if ( $ret >= 200 and $ret < 300 ) {
        print "Download succeeded with return code $ret\n";
        return $file;
    }
    else {
        print "Download failed with return code $ret\n";
        return undef;
    }
}

# Determine a bounding box plus margin around the found stations
sub extractvasttrafikstationsboundingboxfromxml {
    my ($xmlfile) = @_;
    my $dom       = XML::LibXML->new->parse_file($xmlfile);
    my $minlat    = 200;
    my $maxlat    = -200;
    my $minlon    = 200;
    my $maxlon    = -200;
    for my $node ( $dom->findnodes('//StopLocation') ) {
        my $lat = $node->getAttribute('lat');
        my $lon = $node->getAttribute('lon');
        if ( $lat > $maxlat ) { $maxlat = $lat; }
        if ( $lat < $minlat ) { $minlat = $lat; }
        if ( $lon > $maxlon ) { $maxlon = $lon; }
        if ( $lon < $minlon ) { $minlon = $lon; }
    }

    printf "Bounding box of Vasttrafik stations:\n  lat= %.5f  lon= %.5f\n  lat= %.5f  lon= %.5f\n", $minlat, $minlon, $maxlat, $maxlon;

    # Add extra margin to bounding box before returning the coordinates
    return ( $minlon - $margin, $minlat - $margin, $maxlon + $margin, $maxlat + $margin );
}

# Get OSM XML data for bounding box
sub osmxmldataforboundingbox {
    my ( $minlon, $minlat, $maxlon, $maxlat ) = @_;
    my $url = "http://api.openstreetmap.org/api/0.6/map?bbox=$minlon,$minlat,$maxlon,$maxlat";
    print "Fetching URL \"" . $url . "\"\n";

    my $file = "/tmp/osmbb.xml";
    my $ret = mirror( $url, $file );
    if ( $ret >= 200 and $ret < 300 ) {
        print "Download succeeded with return code $ret\n";
        return $file;
    }
    else {
        print "Download failed with return code $ret\n";
        return undef;
    }
}

# Get a hash table of stop locations, their names and their coordinates
sub extractvasttrafikstoplocationstotable {
    my ($xmlfile) = @_;
    my $dom = XML::LibXML->new->parse_file($xmlfile);

    my $mindistance = 100000;
    my $maxdistance = 0;
    my %allstations = ();
    for my $node ( $dom->findnodes('//StopLocation') ) {
        my $id      = $node->getAttribute('id');
        my %station = ();

        # Skip individual tracks, accept only "full" stations
        next if ( defined( $node->getAttribute('track') ) );

        print "Vasttrafik stations\n" if ( scalar( keys %allstations ) == 0 );

        # Remove last name component, which is the muncipality or town
        my @namecomponents = split /, /, $node->getAttribute('name');
        pop @namecomponents;
        $station{name} = join( ", ", @namecomponents );

        $station{lat}     = $node->getAttribute('lat');
        $station{lon}     = $node->getAttribute('lon');
        $allstations{$id} = \%station;
        printstation( \%station );

        # Keep track of distances from starting position
        my $distancefromcenter = $gis->distance( $startlat, $startlon => $station{lat}, $station{lon} )->meters();
        if ( $distancefromcenter > $maxdistance ) { $maxdistance = $distancefromcenter; }
        if ( $distancefromcenter < $mindistance ) { $mindistance = $distancefromcenter; }
    }

    if ( $scopedistance < $maxdistance + 50 ) { $scopedistance = $maxdistance + 50; }

    print "Found  " . scalar( keys %allstations ) . "  Vasttrafik stations in area\n";
    printf "  Distance from start point in meters: %.0f ... %.0f\n", $mindistance, $maxdistance if ( scalar( keys %allstations ) > 0 );
    return %allstations;
}

# Get a hash table of stop locations, their names and their coordinates
sub extractosmstoplocationstotable {
    my ($xmlfile) = @_;
    my $dom = XML::LibXML->new->parse_file($xmlfile);

    my $mindistance = 100000;
    my $maxdistance = 0;
    my %allstations = ();
    for my $node ( $dom->findnodes('//node[tag[@k="highway" and @v="bus_stop"] or tag[@k="amenity" and @v="bus_station"] or tag[@k="railway" and @v="station"] or tag[@k="public_transport" and @v="stop_position"]]') ) {
        my $id      = $node->getAttribute('id');
        my %station = ();

        print "OSM stations\n" if ( scalar( keys %allstations ) == 0 );

        $station{lat} = $node->getAttribute('lat');
        $station{lon} = $node->getAttribute('lon');

        if ( !defined( ( $node->findnodes('tag[@k="name"]/@v') )[0] ) ) {

            # Errors in OSM data: stations/stops without name
            printf STDERR "OSM node $id at lat= %.5f lon= %.5f has no name\n", $station{lat}, $station{lon};
            next;
        }

        $station{name} = ( $node->findnodes('tag[@k="name"]/@v') )[0]->textContent;
        $allstations{$id} = \%station;
        printstation( \%station );

        # Keep track of distances from starting position
        my $distancefromcenter = $gis->distance( $startlat, $startlon => $station{lat}, $station{lon} )->meters();
        if ( $distancefromcenter > $maxdistance ) { $maxdistance = $distancefromcenter; }
        if ( $distancefromcenter < $mindistance ) { $mindistance = $distancefromcenter; }
    }

    print "Found  " . scalar( keys %allstations ) . "  OSM stations in area\n";
    printf "  Distance from start point in meters: %.0f ... %.0f\n", $mindistance, $maxdistance if ( scalar( keys %allstations ) > 0 );
    return %allstations;
}

# Compute geographical distance between every OSM and every Vasttrafik station
sub distancecomputationosmbyvasttrafik {
    my %distancelist = ();
    my ( $allvasttrafikstations, $allosmstations ) = @_;
    for my $vasttrafikid ( keys %$allvasttrafikstations ) {
        my %vasttrafikstation = %{ $allvasttrafikstations->{$vasttrafikid} };
        for my $osmid ( keys %$allosmstations ) {
            my %osmstation   = %{ $allosmstations->{$osmid} };
            my $distance     = $gis->distance( $vasttrafikstation{lat}, $vasttrafikstation{lon} => $osmstation{lat}, $osmstation{lon} )->meters();
            my $id           = $vasttrafikid . "-" . $osmid;
            my %distancehash = ();
            $distancehash{vasttrafikid} = $vasttrafikid;
            $distancehash{osmid}        = $osmid;
            $distancehash{distance}     = $distance;
            $distancelist{$id}          = \%distancehash;
        }
    }
    return %distancelist;
}

my $vasttrafikxmlfile = fetchvasttrafikstationsatcoordinates( $startlat, $startlon );
die("Could not fetch coordinates") unless defined($vasttrafikxmlfile);

my %allvasttrafikstations = extractvasttrafikstoplocationstotable($vasttrafikxmlfile);
die("Could not fetch Vasttrafik stations") unless %allvasttrafikstations;

my @boundingbox = extractvasttrafikstationsboundingboxfromxml($vasttrafikxmlfile);
die("Could not determine stations' bounding box") unless @boundingbox and defined( $boundingbox[3] );

my $osmxmlfile = osmxmldataforboundingbox(@boundingbox);
die("Could not retrieve OSM XML data") unless defined($osmxmlfile);

my %allosmstations = extractosmstoplocationstotable($osmxmlfile);

my %distancelist = distancecomputationosmbyvasttrafik( \%allvasttrafikstations, \%allosmstations );

# Process all pairs of OSM and Vasttrafik stations,
# ordered by inter-pair distance
my %matchedids = ();
for my $dlid ( sort { $distancelist{$a}{distance} <=> $distancelist{$b}{distance} } keys %distancelist ) {
    my $distance       = $distancelist{$dlid}{distance};
    my $vasttrafikname = lc( $allvasttrafikstations{ $distancelist{$dlid}{vasttrafikid} }{name} );
    my $osmname        = lc( $allosmstations{ $distancelist{$dlid}{osmid} }{name} );
    if ( namesdomatch( $vasttrafikname, $osmname ) > 0 ) {
        $matchedids{ $distancelist{$dlid}{vasttrafikid} } = ( $distance > 100 ? -1 : 1 ) * $distancelist{$dlid}{osmid};
        $matchedids{ $distancelist{$dlid}{osmid} }        = ( $distance > 100 ? -1 : 1 ) * $distancelist{$dlid}{vasttrafikid};
    }
}

print "Matched stations:\n";
my $hits = 0;
for my $vasttrafikid ( keys %allvasttrafikstations ) {
    if ( defined( $matchedids{$vasttrafikid} ) and $matchedids{$vasttrafikid} > 1 ) {
        print "\n" if ( $hits > 0 );
        printstation( \%{ $allvasttrafikstations{$vasttrafikid} } );
        print "                         matching\n";
        printstation( \%{ $allosmstations{ $matchedids{$vasttrafikid} } } );
        ++$hits;
    }
}
print "  NONE\n" if ( $hits == 0 );

print "Unmatched Vasttrafik (not found in OSM):\n";
$hits = 0;
for my $vasttrafikid ( keys %allvasttrafikstations ) {
    if ( !defined( $matchedids{$vasttrafikid} ) ) {
        printstation( \%{ $allvasttrafikstations{$vasttrafikid} } );
        print printurl( $allvasttrafikstations{$vasttrafikid}{lat}, $allvasttrafikstations{$vasttrafikid}{lon} ) . "\n";
        ++$hits;
    }
}
print "  NONE\n" if ( $hits == 0 );

print "Unmatched OSM (not found in Vasttrafik):\n";
$hits = 0;
for my $osmid ( keys %allosmstations ) {
    my $distancefromcenter = $gis->distance( $allosmstations{$osmid}{lat}, $allosmstations{$osmid}{lon} => $startlat, $startlon )->meters();
    if ( $distancefromcenter < $scopedistance and !defined( $matchedids{$osmid} ) ) {
        printstation( \%{ $allosmstations{$osmid} } );
        print printurl( $allosmstations{$osmid}{lat}, $allosmstations{$osmid}{lon} ) . "\n";
        ++$hits;
    }
}
print "  NONE\n" if ( $hits == 0 );

print "Too far away Vasttrafik station:\n";
$hits = 0;
for my $vasttrafikid ( keys %allvasttrafikstations ) {
    my $distancefromcenter = $gis->distance( $allvasttrafikstations{$vasttrafikid}{lat}, $allvasttrafikstations{$vasttrafikid}{lon} => $startlat, $startlon )->meters();
    if ( $distancefromcenter < 1000 and defined( $matchedids{$vasttrafikid} ) and $matchedids{$vasttrafikid} < -1 ) {
        printstation( \%{ $allvasttrafikstations{$vasttrafikid} } );
        print printurl( $allvasttrafikstations{$vasttrafikid}{lat},              $allvasttrafikstations{$vasttrafikid}{lon} ) . "\n";
        print printurl( $allosmstations{ -1 * $matchedids{$vasttrafikid} }{lat}, $allosmstations{ -1 * $matchedids{$vasttrafikid} }{lon} ) . "\n";
        ++$hits;
    }
}
print "  NONE\n" if ( $hits == 0 );

print "Too far away OSM station:\n";
$hits = 0;
for my $osmid ( keys %allosmstations ) {
    my $distancefromcenter = $gis->distance( $allosmstations{$osmid}{lat}, $allosmstations{$osmid}{lon} => $startlat, $startlon )->meters();
    if ( $distancefromcenter < 1000 and defined( $matchedids{$osmid} ) and $matchedids{$osmid} < -1 ) {
        printstation( \%{ $allosmstations{$osmid} } );
        print printurl( $allosmstations{$osmid}{lat},                            $allosmstations{$osmid}{lon} ) . "\n";
        print printurl( $allvasttrafikstations{ -1 * $matchedids{$osmid} }{lat}, $allvasttrafikstations{ -1 * $matchedids{$osmid} }{lon} ) . "\n";
        ++$hits;
    }
}
print "  NONE\n" if ( $hits == 0 );
